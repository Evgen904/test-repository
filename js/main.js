$(function(){

	function menuTop(menu, line) {
		var positionLeft = $(menu).find('.active').position().left;
		var widthActive = $(menu).find('.active').outerWidth();
		$(line).animate({'left': positionLeft, 'width': widthActive});	
		$('.menu-top-tabs li').on('click',function(){
			$(menu).removeClass('first-active');
			$(menu).find('li').removeClass('active');
			$(this).addClass('active');
			default_left = $(this).position().left;
			default_width = $(this).outerWidth();
			$(line).animate({'left': default_left,'width': default_width});
		});
	};
	menuTop('.menu-top-tabs','.line-active-menu');

	var texts = $('.tabs-wr>div'),
		tabs = $('.menu-top-tabs li').on('click',function(){
			var $this = $(this),
				index = $this.index();
			tabs.removeClass('active');
			$this.addClass('active');
			texts.removeClass('active');
			texts.eq(index).addClass('active');
		});

	$('.spoiler-btn span').on('click',function(){
		if (!$(this).parent().hasClass('active')) {
			$(this).parent().addClass('active');
		}
		else {
			$(this).parent().removeClass('active');
		}
	});

	

});